import path from 'path';
import * as fs from 'node:fs';
import vue from '@vitejs/plugin-vue';
import autoprefixer from 'autoprefixer';
import { defineConfig } from 'vite';
import { getManifest } from './src/manifest.js';

function root(...paths) {
  return path.resolve(__dirname, ...paths);
}

export default defineConfig(({ mode }) => {
  return {
    mode,
    root: root('src'),
    publicDir: root('public'),
    assetsInclude: ['**/*.md'],
    build: {
      outDir: root('dist'),
      emptyOutDir: true,
      rollupOptions: {
        input: {
          page: root('src/entries/page/index.html'),
          options: root('src/entries/options/index.html'),
          help: root('src/entries/help/index.html')
        },
        output: {
          manualChunks: {
            'vendor': ['@fortawesome/fontawesome-svg-core', '@fortawesome/free-solid-svg-icons', '@fortawesome/vue-fontawesome']
          },
          assetFileNames: assetInfo => {
            if (/\.(png|jpg|jpeg|gif|svg)$/i.test(assetInfo.name)) {
              return 'images/[name][extname]';
            }
            if (/\.(woff2?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/i.test(assetInfo.name)) {
              return 'fonts/[name][extname]';
            }
            return 'assets/[name][extname]';
          }
        }
      }
    },
    plugins: [
      vue(),
      {
        name: 'manifest-json',
        writeBundle: (options, _bundle) => {
          const manifest = getManifest(mode);
          fs.writeFileSync(root('dist', 'manifest.json'), JSON.stringify(manifest, null, 2));
        }
      }
    ],
    css: {
      postcss: {
        plugins: [
          autoprefixer
        ]
      }
    }
  };
});
