# Changelog

This changelog will contain only intermediate changes between major releases

## 1.0.0-beta.6
* Migrate to Vite + yarn
* Build signed XPI extensions for Firefox
* Add settings import/export

## 1.0.0-beta.5
* Migrate to Vue.js 3

## 1.0.0-beta.4
* Added help page stub
* Settings page partly implemented: added ability to edit folders configuration
* Fixed links with cyrillic labels not being searchable
* Updated vulnerable dependencies
* Updated Font Awesome to 6.2.0

## 1.0.0-beta.3
* Fixed searching for custom environment forms not working properly
* Added autocompletion of search query by pressing Tab

## 1.0.0-beta.2
* Internal search extended: results with query string anywhere inside the result label will be also displayed

## 1.0.0-beta.1
* Initial beta release (some features are still missing, see [TODO.md](TODO.md))
