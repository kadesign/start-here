# TODO

- [x] ~~Replace Google search bar with internal search bar which opens configured links~~                                                                                        >
  > Commands:
  > - `<environment name>` - open entry link to environment
  > - `<environment name> <link name or starting chars>` - concrete link to environment (with suggestions)
  > - `!env <...>` - same as above but exclude other options
  > - `<link name or starting chars>` - open link from links folder
  > - `!link <...>` - same as above but exclude environments
  > - `!h` - help

- [x] ~~Mark link type in suggestions (might be improved in the future)~~
- [x] ~~Use Fetch API instead of Axios~~
- [x] ~~Get rid of lodash (1 usage)~~
- [x] ~~Clean up local storage logic and tests~~
- [x] ~~Remove migration feature after a while~~
- [x] ~~Share styles of suggestion box between components~~
- [x] ~~Rename "link list/folder" to "bookmark list/folder" and prefix !l/!link to !b/!bm (leave prefix as is maybe?)~~
- [ ] Settings page
- [ ] Switch between Google search and internal search (switch to internal search by default)
- [ ] Icons
- [ ] Help page/popup
- [ ] After publishing: correct all links in README

**=== V1 ENDS HERE ===**

- [ ] Dark mode (as in system)
- [ ] Remedy/non-Remedy environments
- [ ] Search mode only (without visible folders)
- [ ] Make suggestion box scrollable (?)
- [ ] Localizations - https://kazupon.github.io/vue-i18n/ru/
- [ ] First start guide (if no configuration is present)
- [ ] Drag and drop bookmarks between lists (between folders also?) / reorder inside list
- [ ] Reorder folders
