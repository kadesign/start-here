# Changelog

## 1.0.0 (YYYY-MM-DD)
✨ **Initial release of StartHere!**

🚀 **Features:**
- Configure links to all your BMC Remedy environments and gain convenient access to them.
- Create folders and group your favorite links.
- Pin up to five links to the dedicated QuickAccess area.
- Search for necessary links using a handy search bar or just switch it to the Google search bar.

⛳️ **Planned for upcoming releases:**
- Dark mode! Turn it on and off automatically or whenever you want.
- Configure custom non-Remedy environments if you have some.
- Localizations — Russian, German, etc.
- Search-only mode – hides folders from the page, so all configured links will be available only via search.