import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faFolderPlus,
  faFloppyDisk,
  faServer,
  faStar,
  faCaretRight,
  faPenToSquare,
  faCirclePlus,
  faTrashCan,
  faCircleXmark,
  faAdd
} from '@fortawesome/free-solid-svg-icons';

import Main from './components/Main.vue';

library.add(
  faFolderPlus,
  faFloppyDisk,
  faServer,
  faStar,
  faCaretRight,
  faPenToSquare,
  faCirclePlus,
  faTrashCan,
  faCircleXmark,
  faAdd
);
const pinia = createPinia();

createApp(Main)
  .component('font-awesome-icon', FontAwesomeIcon)
  .provide('$configKey', 'sh_config')
  .use(pinia)
  .mount('#app');
