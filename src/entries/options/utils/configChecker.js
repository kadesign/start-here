const validate = config => {
  let result = true;

  // Folders
  for (let folder of config.folders) {
    // Should have label filled
    if (!folder.label) {
      result = false;
      break;
    }

    // Check folder contents
    switch (folder.type) {
      case 'environment':
        result = validateEnvFolder(folder);
        break;
      case 'list':
        result = validateBookmarkFolder(folder);
        break;
      default:
        result = false;
        break;
    }
    if (!result) break;

    // TODO: check QuickAccess and miscellaneous settings
  }
  return result;
};

const validateBookmarkFolder = folder => {
  for (let item of folder.items) {
    if (!item.label) return false;

    for (let link of item.links) {
      if (!link.label || !link.url) return false;
    }
  }

  return true;
};

const validateEnvFolder = folder => {
  for (let item of folder.items) {
    if (!item.environment || !item.label || !item.url || !item.alias) return false;

    for (let schema of item.selectedSchemas) {
      if (!schema.label || !schema.schemaName) return false;
    }
  }

  return true;
};

export default {
  validate
};
