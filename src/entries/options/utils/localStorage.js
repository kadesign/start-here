const get = key => {
  return new Promise((resolve, reject) => {
    browser.storage.local.get([key], items => {
      if (browser.runtime.lastError) {
        console.error(browser.runtime.lastError);
        reject(browser.runtime.lastError.message);
      } else {
        resolve(items[key]);
      }
    });
  });
};

const set = (key, value) => {
  return new Promise((resolve, reject) => {
    const newValue = JSON.parse(JSON.stringify(value));
    browser.storage.local.set({ [key]: newValue }, () => {
      if (browser.runtime.lastError) {
        console.error(browser.runtime.lastError);
        reject(browser.runtime.lastError.message);
      } else {
        resolve();
      }
    });
  });
};

export default {
  get,
  set
};
