import { inject, nextTick, ref, watch } from 'vue';
import { defineStore } from 'pinia';
import { v4 } from 'uuid';
import configChecker from '../utils/configChecker.js';
import localStorage from '../utils/localStorage.js';

export const useSettingsStore = defineStore('settingsStore', () => {
  const $configKey = inject('$configKey');

  const loading = ref(false);
  const saved = ref(true);
  const message = ref('');
  const folders = ref([]);
  const quickAccess = ref([]);

  watch([folders.value, quickAccess.value], () => {
    if (!loading.value) {
      message.value = 'There are unsaved changes.';
      saved.value = false;
    }
  });

  const addFolder = folder => {
    folder.id = v4();
    folders.value.push(folder);
  };

  const removeFolder = id => {
    const folder = folders.value.findIndex(f => f.id === id);
    if (folder >= 0) {
      folders.value.splice(folder, 1);
    }
  }

  const addQuickAccess = () => {
  };

  const setMessage = msg => {
    message.value = msg;
  }

  const persist = async () => {
    const data = {
      folders: folders.value,
      quickAccess: quickAccess.value
    };
    if (configChecker.validate(data)) {
      try {
        await localStorage.set($configKey, data);
      } catch (e) {
        console.error(e);
        message.value = 'Something went wrong...';
      }
      saved.value = true;
    } else {
      message.value = 'Some required fields are not filled!';
    }
  };

  const load = async () => {
    loading.value = true;

    const savedData = await localStorage.get($configKey);
    applyData(savedData);
    saved.value = true;
    await nextTick();

    loading.value = false;
  };

  const importFromDump = dump => {
    if (configChecker.validate(dump)) {
      applyData(dump);
    } else {
      message.value = 'Some required fields are not filled';
    }
  };

  const applyData = (data) => {
    folders.value.length = 0;
    data.folders.forEach(folder => {
      if (!folder.id) {
        folder.id = v4();
      }
    });
    folders.value.push(...data.folders);
    quickAccess.value.length = 0;
    quickAccess.value.push(...data.quickAccess);
    saved.value = false;
  }

  return {
    folders,
    quickAccess,
    saved,
    message,

    addFolder,
    removeFolder,
    addQuickAccess,
    setMessage,

    persist,
    load,
    importFromDump
  };
});
