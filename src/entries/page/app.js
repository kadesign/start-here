import { createApp } from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faServer, faSearch, faChevronDown, faStar } from '@fortawesome/free-solid-svg-icons';

import Main from './components/Main.vue';

library.add(faServer, faSearch, faChevronDown, faStar);

createApp(Main)
  .component('font-awesome-icon', FontAwesomeIcon)
  .provide('$configKey', 'sh_config')
  .mount('#app');
