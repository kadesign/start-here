const constructHomeLink = rootUrl => {
  return `${rootUrl}/arsys/home`;
};
const constructObjListLink = (rootUrl, alias) => {
  return `${rootUrl}/arsys/forms/${alias}/AR+System+Mid+Tier+Object+List/`;
};
const constructMidTierConfigLink = rootUrl => {
  return `${rootUrl}/arsys/shared/config/config.jsp`;
};
const constructAdminConsoleLink = (rootUrl, alias) => {
  return `${rootUrl}/arsys/forms/${alias}/AR+System+Administration%3A+Console/`;
};
const constructObjReservationLink = (rootUrl, alias) => {
  return `${rootUrl}/arsys/forms/${alias}/AR+System+Version+Control%3A+Object+Reservation/`;
};
const constructSchemaLink = (rootUrl, alias, schemaName) => {
  return `${rootUrl}/arsys/forms/${alias}/${encodeURIComponent(schemaName)}/`;
}

export default {
  constructHomeLink,
  constructObjListLink,
  constructMidTierConfigLink,
  constructAdminConsoleLink,
  constructObjReservationLink,
  constructSchemaLink
}