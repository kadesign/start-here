import arsLinksConstructor from './arsLinksConstructor.js';

const sanitizeQuery = query => {
  return query
    .toLowerCase()
    .replaceAll(/[^a-zа-я0-9 ]+/g, '')
    .replaceAll(/ +/g, ' ')
    .trim();
};

const build = foldersConfig => {
  const index = [];
  const folders = foldersConfig.sort((a, b) => a.order - b.order);

  const environmentFolders = folders.filter(folder => folder.type === 'environment');
  const listFolders = folders.filter(folder => folder.type === 'list');

  environmentFolders.forEach(folder => {
    folder.items.forEach(env => {
      index.push(
        {
          url: arsLinksConstructor.constructHomeLink(env.url),
          displayName: `${env.environment} (${env.label})`,
          label: folder.label,
          query: sanitizeQuery(env.environment),
          subquery: '',
          type: folder.type,
          isHome: true
        },
        {
          url: arsLinksConstructor.constructObjListLink(env.url, env.alias),
          displayName: 'Object List',
          label: `${folder.label} / ${env.environment} / Common`,
          query: sanitizeQuery(env.environment),
          subquery: 'object list',
          type: folder.type
        },
        {
          url: arsLinksConstructor.constructMidTierConfigLink(env.url),
          displayName: 'Mid-Tier Config',
          label: `${folder.label} / ${env.environment} / Common`,
          query: sanitizeQuery(env.environment),
          subquery: 'mid tier config',
          type: folder.type
        },
        {
          url: arsLinksConstructor.constructAdminConsoleLink(env.url, env.alias),
          displayName: 'Admin Console',
          label: `${folder.label} / ${env.environment} / Common`,
          query: sanitizeQuery(env.environment),
          subquery: 'admin console',
          type: folder.type
        }
      );
      if (env.objectReservation) {
        index.push({
          url: arsLinksConstructor.constructObjReservationLink(env.url, env.alias),
          displayName: 'Object Reservation',
          label: `${folder.label} / ${env.environment} / Common`,
          query: sanitizeQuery(env.environment),
          subquery: 'object reservation',
          type: folder.type
        });
      }
      if (env.arinsideRoot) {
        index.push({
          url: env.arinsideRoot,
          displayName: 'ARInside',
          label: `${folder.label} / ${env.environment} / Common`,
          query: sanitizeQuery(env.environment),
          subquery: 'arinside',
          type: folder.type
        });
      }
      if (env.selectedSchemas.length > 0) {
        env.selectedSchemas.forEach(schema => {
          index.push({
            url: arsLinksConstructor.constructSchemaLink(env.url, env.alias, schema.schemaName),
            displayName: `${schema.label}`,
            label: `${folder.label} / ${env.environment} / Forms`,
            query: sanitizeQuery(env.environment),
            subquery: sanitizeQuery(schema.label),
            type: folder.type
          });
        });
      }
    });
  });

  let listIndex = [];
  listFolders.forEach(folder => {
    folder.items.forEach(list => {
      list.links.forEach(link => {
        listIndex.push({
          url: link.url,
          displayName: link.label,
          label: `${folder.label} / ${list.label}`,
          query: sanitizeQuery(link.label),
          subquery: '',
          type: folder.type
        });
      });
    });
  });
  listIndex = listIndex.sort((a, b) => a.query.localeCompare(b.query));

  return index.concat(listIndex);
};

const fetchResults = (index, query, mode) => {
  const results = new Set();

  if (query !== '') {
    switch (mode) {
      case 'env': {
        const queryStart = query.substr(0, query.indexOf(' ') > 0 ? query.indexOf(' ') : undefined);
        const subquery = query.indexOf(' ') > 0 ? query.substr(query.indexOf(' ') + 1) : null;

        if (!subquery) {
          index.filter(item => item.query.startsWith(queryStart) && item.type === 'environment' && item.isHome).forEach(results.add, results);
          index.filter(item => item.query.includes(queryStart) && item.type === 'environment' && item.isHome).forEach(results.add, results);
        } else {
          index.filter(item => item.query === queryStart && item.subquery.startsWith(subquery) && item.type === 'environment').forEach(results.add, results);
          index.filter(item => item.query === queryStart && item.subquery.includes(subquery) && item.type === 'environment').forEach(results.add, results);
        }
        break;
      }
      case 'bookmark':
        index.filter(item => item.query.startsWith(query) && item.type === 'linkList').forEach(results.add, results);
        index.filter(item => item.query.includes(query) && item.type === 'linkList').forEach(results.add, results);
        break;
      default: {
        const queryStart = query.substr(0, query.indexOf(' ') > 0 ? query.indexOf(' ') : undefined);
        const subquery = query.indexOf(' ') > 0 ? query.substr(query.indexOf(' ') + 1) : null;

        if (!subquery) {
          index.filter(item => item.query.startsWith(queryStart) && (item.isHome || item.type !== 'environment')).forEach(results.add, results);
          index.filter(item => item.query.includes(queryStart) && (item.isHome || item.type !== 'environment')).forEach(results.add, results);
        } else {
          index.filter(item => item.query === queryStart && item.subquery.startsWith(subquery) && item.type === 'environment').forEach(results.add, results);
          index.filter(item => item.query.startsWith(`${queryStart} ${subquery}`) && item.type !== 'environment').forEach(results.add, results);
          index.filter(item => item.query === queryStart && item.subquery.includes(subquery) && item.type === 'environment').forEach(results.add, results);
          index.filter(item => item.query.includes(`${queryStart} ${subquery}`) && item.type !== 'environment').forEach(results.add, results);
        }
        break;
      }
    }
  }

  return results;
};

export default {
  build,
  sanitizeQuery,
  fetchResults
};
