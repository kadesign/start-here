# Main GUI {gui}

__StartHere__ consists of three functional parts - __Folders__, __QuickAccess__ and __Search bar__.

## Folders {gui-folders}

*Folders* are essentially lists or collections of links. They are displayed in top left corner of the tab.

There are currently two types of Folders:

- __Environment__: represents BMC Remedy ARS (Action Request System) environment. It contains links to commonly used built-in forms and to user-defined forms.
- __Bookmarks__: contains one or more collections of configurable bookmarks (not to be confused with Google Chrome bookmarks).

You can create up to 6 folders.

## QuickAccess {gui-quickaccess}

*QuickAccess* is a collection of frequently used bookmarks which meant to be accessible in one click. They are located in the top right corner of the tab.

Unlike links in Folders, QuickAccess links have their own icons.

## Search bar {gui-search-bar}

*Search bar* can be found in the middle of the tab. There are two modes of Search bar:

- __Internal search__ (default): allows searching for a link in user's Folders or opening extension's pages (Help, Settings) in "command line"-like way.
- __Google Search__: duplicates functionality of the search bar from default Google Chrome's new tab page.

### Using internal search {gui-sb-internal}

TODO

# Configuration {configuration}

TODO

## Folders {conf-folders}

TODO

### Environments {conf-fld-environments}

TODO

### Bookmarks {conf-fld-bookmarks}

TODO

## QuickAccess {conf-quickaccess}

TODO

## Miscellaneous {conf-miscellaneous}

TODO