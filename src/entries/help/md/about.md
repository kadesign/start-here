# StartHere <div class="version">[[VERSION]]</div>
Simple and minimalistic Chrome's New tab page replacement for BMC Remedy ARS developers.

Designed by [__Danil Katashev__](https://danilkatashev.ru) and licensed under [MIT License](https://opensource.org/licenses/MIT).
<br><br>

## Changelog
Full changelog can be found [here](https://gitlab.com/kadesign/start-here/-/blob/master/CHANGELOG.md).

## Links
* [Source code](https://gitlab.com/kadesign/start-here)
* [Bug tracker](https://gitlab.com/kadesign/start-here/-/issues)

## Attributions
* __Icons:__ [Font Awesome Free 5.10.2](https://fontawesome.com) <br>
License - https://fontawesome.com/license/free (Icons: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), Fonts: [SIL OFL 1.1](https://scripts.sil.org/OFL), Code: [MIT License](https://opensource.org/licenses/MIT))
* __Font:__ [Roboto by Google](https://fonts.google.com/specimen/Roboto) <br>
License - [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)