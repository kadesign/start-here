import { createApp } from 'vue';
import Main from './components/Main.vue';

createApp(Main)
  .mount('#app');
