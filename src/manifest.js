import pkg from "../package.json";

const manifest = {
  icons: {
    48: 'icons/48.png',
    128: 'icons/128.png'
  },
  browser_specific_settings: {
    gecko: {
      id: '{bc7cb468-488c-4eed-8d5d-c752c8b33ad8}'
    }
  },
  chrome_url_overrides: {
    newtab: 'entries/page/index.html'
  },
  options_ui: {
    open_in_tab: true,
    page: 'entries/options/index.html',
  },
  permissions: ['storage'],
};

export function getManifest(mode) {
  console.log(mode);
  const finalManifest = {
    manifest_version: 3,
    name: pkg.displayName ?? pkg.name,
    version: pkg.version.replace('-beta', ''),
    version_name: pkg.version + (mode === 'development' ? ' (development build)' : ''),
    description: pkg.description,
    ...manifest
  };
  if (mode === 'development') {
    finalManifest.content_security_policy = "script-src 'self' 'unsafe-eval'; object-src 'self'";
  }

  return finalManifest;
}
