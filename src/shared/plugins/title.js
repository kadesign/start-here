import { ref, watch } from 'vue';

export const useTitle = appTitle => {
  const pageTitle = ref('');

  function updateTitle() {
    document.title = `${pageTitle.value ? pageTitle.value + ' - ' : ''}${appTitle}`;
  }

  watch(pageTitle, () => updateTitle());

  return { pageTitle };
};
