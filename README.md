<div align="center">
    <h1>StartHere</h1>
    <p>Simple and minimalistic New tab page replacement for BMC Remedy ARS developers</p>
    <p>
        <img alt="Maintenance status" src="https://img.shields.io/maintenance/yes/2024">
        <a href="https://gitlab.com/kadesign/start-here/-/releases/v1.0.0-beta.6"><img src="https://img.shields.io/badge/version-v1.0.0--beta.6-blue" alt="Version"></a>
        <a href="https://gitlab.com/kadesign/start-here/-/commits/master"><img alt="Pipeline status" src="https://gitlab.com/kadesign/start-here/badges/master/pipeline.svg"></a>
    </p>
</div>

## Key features

- Configure links to all your BMC Remedy environments and gain convenient access to them.
- Create folders and group your favorite links.
- Pin up to five links to the dedicated QuickAccess area.
- Search for necessary links using a handy search bar or just switch it to the Google search bar.

**Note:** the links configured in this extension and browser bookmarks will not be synchronized. Always back up your configuration!

## How to install

### Firefox
Get latest version from [Releases](https://gitlab.com/kadesign/start-here/-/releases) and open.

### Chrome
- Download the latest ZIP build from [Releases](https://gitlab.com/kadesign/start-here/-/releases).
- Unzip the archive and place the directory `start-here` to any desired location. Do not delete the directory unless you don't need the extension anymore!
- Go to the Extensions page (`chrome://extensions`) and enable Developer mode.
- Drag the `start-here` folder anywhere on the page to import the extension.

## How to use

Type `!h` in the search bar inside the extension to open the help page.

## Changelog

You can find full changelog for beta releases [here](CHANGELOG-BETA.md). 

## Bugs and feature requests

Have a bug or a feature request? Please first search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.com/kadesign/start-here/-/issues/new).

## Copyright and license

Copyright 2020 [Danil Katashev](mailto:dev@danilkatashev.ru). Released under the [MIT License](LICENSE).
